import React, {useState} from 'react';
import './App.css';
import {Link, BrowserRouter as Router, Route} from "react-router-dom";
import Form from "./components/Form";
import Main from "./components/Main";
import News from "./components/News";
import Profile from "./components/Profile";
import Menu from "./components/Menu/Menu";

const App = () => {

    const [menuActive, setMenuActive] = useState(false)
    const items = [
        {value: "Main", href: '/'},
        {value: "Login", href: '/login'},
        {value: "News", href: '/news'},
        {value: "Profile", href: '/profile'},
    ]

    return (
        <section className="App">
            <div className='app'>
                <nav>
                    <div className="main-menu-btn" onClick={() => setMenuActive(!menuActive)}>
                        <span></span>
                    </div>
                </nav>
                <main>
                    <div>
                        {<Menu active={menuActive} setActive={setMenuActive} header={"Menu"} items={items}/>}
                    </div>
                    <div>
                        <Router>
                            <Link to="/"></Link>
                            <Link to="/login"></Link>
                            <Link to="/news"></Link>
                            <Link to="/profile"></Link>
                            <Route exact path="/" component={Main} />
                            <Route exact path="/login" component={Form} />
                            <Route exact path="/news" component={News} />
                            <Route exact path="/profile" component={Profile}/>
                        </Router>
                    </div>
                </main>
            </div>


        </section>
    );
};
export default App;
