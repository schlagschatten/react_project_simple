import React from 'react';
import './Menu.css'

const Menu = ({header, items, active, setActive}) => {
    console.log(items);

    const checkKey = (item) => {
        if (item.value === "Profile" && localStorage.getItem(process.env.REACT_APP_EMAIL) === null) {
            window.location.href = "/login"
        }else {
            window.location.href = item.href
        }
    }

    return (
        <div className={active ? 'menu active' : 'menu'} onClick={() => setActive(false)}>
          <div className='blur'/>
              <div className='menu_content' onClick={e => e.stopPropagation()}>
                  <div className='menu_header'>{header}</div>
                  <ul>
                      {items.map(item =>
                      <li>
                          <a style={{cursor: "pointer"}} onClick={e => checkKey(item)}>{item.value}</a>
                      </li>
                      )}
                  </ul>
              </div>
        </div>
    );
};

export default Menu;